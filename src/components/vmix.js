// This file makes API calls directly to vMix https://www.vmix.com/help23/index.htm?DeveloperAPI.html
// Need help building a script line? https://forums.vmix.com/posts/t18559-vMix-Script-Builder-App---Updated-02-24-20
class vMixIntegration {
    vmixWebControllerEnabled = false;
    vmixBase = "http://localhost:8088/API?";

    constructor() {
        let _this = this;
        $.get(this.vmixBase, () => {
            ToastController.show("vMix Web Controller", "Connection Successful!", "success", 10000);
            _this.vmixWebControllerEnabled = true;
        }).fail(() => {
            ToastController.show("vMix Web Controller", "Connection Failed", "danger", 20000);
        });
    }

    runCommand(command) {
        let _this = this;
        return new Promise(((resolve, reject) => {
            if (this.vmixWebControllerEnabled) {
                let parts = command.split('&');
                _.each(parts, (d, i) => {
                    if (d.indexOf('Value') === 0) {
                        let sides = d.split('=');
                        sides[1] = encodeURIComponent(sides[1]);
                        parts[i] = sides.join("=");
                    }
                });
                command = parts.join("&");

                $.get(_this.vmixBase + command, () => {
                    resolve();
                }).fail((resp) => {
                    reject(resp);
                });
            } else {
                reject("vMix WebController not connected");
            }
        }));
    }

    updatePostGameStatsLeaderboard = _.throttle(function(teamData, playersLeft, playersRight) {
        vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-Score.Text&Value=${teamData.left.score}`);
        vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-Team+Name.Text&Value=${teamData.left.name}`);

        if (!_.isUndefined(playersLeft)) {
            try {
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P1-Name.Text&Value=${playersLeft[0].name}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P1-Score.Text&Value=${playersLeft[0].score}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P1-Goals.Text&Value=${playersLeft[0].goals}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P1-Assists.Text&Value=${playersLeft[0].assists}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P1-Saves.Text&Value=${playersLeft[0].saves}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P1-Shots.Text&Value=${playersLeft[0].shots}`).catch(()=>{});

               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P2-Name.Text&Value=${playersLeft[1].name}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P2-Score.Text&Value=${playersLeft[1].score}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P2-Goals.Text&Value=${playersLeft[1].goals}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P2-Assists.Text&Value=${playersLeft[1].assists}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P2-Saves.Text&Value=${playersLeft[1].saves}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P2-Shots.Text&Value=${playersLeft[1].shots}`).catch(()=>{});

               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P3-Name.Text&Value=${playersLeft[2].name}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P3-Score.Text&Value=${playersLeft[2].score}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P3-Goals.Text&Value=${playersLeft[2].goals}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P3-Assists.Text&Value=${playersLeft[2].assists}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P3-Saves.Text&Value=${playersLeft[2].saves}`).catch(()=>{});
               vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=L-P3-Shots.Text&Value=${playersLeft[2].shots}`).catch(()=>{});
            } catch (e) {

            }
        }

        // RIGHT
        vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-Score.Text&Value=${teamData.right.score}`);
        vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-Team+Name.Text&Value=${teamData.right.name}`);

        if (!_.isUndefined(playersRight)) {
            try {
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P1-Name.Text&Value=${playersRight[0].name}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P1-Score.Text&Value=${playersRight[0].score}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P1-Goals.Text&Value=${playersRight[0].goals}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P1-Assists.Text&Value=${playersRight[0].assists}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P1-Saves.Text&Value=${playersRight[0].saves}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P1-Shots.Text&Value=${playersRight[0].shots}`).catch(()=>{});

                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P2-Name.Text&Value=${playersRight[1].name}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P2-Score.Text&Value=${playersRight[1].score}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P2-Goals.Text&Value=${playersRight[1].goals}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P2-Assists.Text&Value=${playersRight[1].assists}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P2-Saves.Text&Value=${playersRight[1].saves}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P2-Shots.Text&Value=${playersRight[1].shots}`).catch(()=>{});

                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P3-Name.Text&Value=${playersRight[2].name}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P3-Score.Text&Value=${playersRight[2].score}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P3-Goals.Text&Value=${playersRight[2].goals}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P3-Assists.Text&Value=${playersRight[2].assists}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P3-Saves.Text&Value=${playersRight[2].saves}`).catch(()=>{});
                vMix.runCommand(`Function=SetText&Input=PostGameStats+Title&SelectedName=R-P3-Shots.Text&Value=${playersRight[2].shots}`).catch(()=>{});
            } catch (e) {

            }
        }
    }, 1000);

    teamGoalAnimation(side) {
        this.runCommand(`Function=Play&Input=Goal_${side}_Stinger`);
    }

    updateUpNextTeams(d) {
        //Highlights Intermission Roster
        this.runCommand(`Function=SetText&Input=HighlightsIntermission+Title&SelectedName=T-Player+1.Text&Value=${d.left.players.one.name}`);
        this.runCommand(`Function=SetText&Input=HighlightsIntermission+Title&SelectedName=T-Player+2.Text&Value=${d.left.players.two.name}`);
        this.runCommand(`Function=SetText&Input=HighlightsIntermission+Title&SelectedName=T-Player+3.Text&Value=${d.left.players.three.name}`);
        this.runCommand(`Function=SetText&Input=HighlightsIntermission+Title&SelectedName=B-Player+1.Text&Value=${d.right.players.one.name}`);
        this.runCommand(`Function=SetText&Input=HighlightsIntermission+Title&SelectedName=B-Player+2.Text&Value=${d.right.players.two.name}`);
        this.runCommand(`Function=SetText&Input=HighlightsIntermission+Title&SelectedName=B-Player+3.Text&Value=${d.right.players.three.name}`);
    }

    updateIntermissionTimers(timeFormat) {
        this.runCommand(`Function=SetText&Input=MainIntermission+Title&Value=${timeFormat}&SelectedName=Timer-Time.Text`);
        this.runCommand(`Function=SetText&Input=HighlightsIntermission+Title&Value=${timeFormat}&SelectedName=Timer.Text`);
    }

    triggerEndOfSeriesVideo() {
        this.runCommand(`Function=OverlayInput3In&Input=Series+Winner+Composite`);
    }

    triggerEndOfMatchTransition() {
        this.runCommand(`Function=OverlayInput4In&Input=Post+Game+Stats+Transition`);
    }

    triggerScorebugReveal() {
        let _this = this;
        this.runCommand(`Function=Play&Input=Scorebug+Reveal`);
        setTimeout(function () {
            _this.runCommand(`Function=MultiViewOverlayOn&Input=THIS.+IS.+ROCKET+LEAGUE&Value=1`);
        }, 1000);
    }

    triggerHideScorebug() {
        this.runCommand(`Function=MultiViewOverlayOff&Input=THIS.+IS.+ROCKET+LEAGUE&Value=1`);
    }

    triggerEnterReplay() {
        this.runCommand(`Function=MultiViewOverlayOn&Input=THIS.+IS.+ROCKET+LEAGUE&Value=10`);
    }

    triggerExitReplay() {
        this.runCommand(`Function=MultiViewOverlayOff&Input=THIS.+IS.+ROCKET+LEAGUE&Value=10`);
    }

    triggerPlayEndMusic() {
        this.runCommand(`Function=NextItem&Input=RL+Match+End+Music`).then(() => {
            this.runCommand(`Function=Play&Input=RL+Match+End+Music`);
        });
    }
}

const vMix = new vMixIntegration();