class StartingIntermissionTab {
    id = "Intermission";
    headerText = "Intermission";
    sortPriority = 2;

    binds = {
        _this: this,
        timer: {
            time: "",
            handleCommit: function (e, b) {
                b._this.handleTimerCommit();
            }
        }
    };

    template() {
        return `
            <div class="card">
                <div class="card-header">
                    Intermission Controls
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    Timer Controls
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="timer-time">Time to Count (seconds)</label>
                                        <input type="number" class="form-control" id="timer-time" name="timer-time" rv-value="timer.time" placeholder="Time to Count (seconds)">
                                    </div>
                                    <button class="btn btn-primary float-right" rv-on-click="timer.handleCommit">Commit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }

    entry() {
    }

    handleTimerCommit() {
        let date = new Date();
        date.setSeconds(date.getSeconds() + _.toInteger(this.binds.timer.time));

        vMix.updateIntermissionTimers(`{0:${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}|mm:ss}`);
    }
}
$(function () {
    TabsController.register(StartingIntermissionTab);
});