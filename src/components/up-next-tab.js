class UpNextTab {
    id = "UpNext";
    headerText = "Up Next";

    binds = {
        _this: this,
        upNext: {
            handleCommit: function (e, b) {
                b._this.handleCommitUpUpTeams();
            }
        },
        teams: {
            left: {
                name: "",
                abbreviation: "",
                players: {
                    one: {
                        name: "",
                    },
                    two: {
                        name: "",
                    },
                    three: {
                        name: "",
                    },
                }
            },
            right: {
                name: "",
                abbreviation: "",
                players: {
                    one: {
                        name: "",
                    },
                    two: {
                        name: "",
                    },
                    three: {
                        name: "",
                    },
                }
            }
        },
    };

    template() {
        return `
            <div class="card">
                <div class="card-header">
                    Up next <small>(Controls what teams are up next on various elements)</small>    
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    Left Team (Blue)
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="team-left-name">Name</label>
                                        <input type="text" class="form-control" id="team-left-name" name="team-left-name" rv-value="teams.left.name" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="team-left-abbreviation">Abbreviation</label>
                                        <input type="text" class="form-control" id="team-left-abbreviation" name="team-left-abbreviation" rv-value="teams.left.abbreviation" placeholder="Abbreviation">
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            Roster
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="team-left-player1">Player 1</label>
                                                        <input type="text" class="form-control" id="team-left-player1" name="team-left-player1" rv-value="teams.left.players.one.name" placeholder="Player 1">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="team-left-player2">Player 2</label>
                                                        <input type="text" class="form-control" id="team-left-player2" name="team-left-player2" rv-value="teams.left.players.two.name" placeholder="Player 2">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="team-left-player3">Player 3</label>
                                                        <input type="text" class="form-control" id="team-left-player3" name="team-left-player3" rv-value="teams.left.players.three.name" placeholder="Player 3">
                                                    </div>  
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    Right Team (Orange)
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="team-right-name">Name</label>
                                        <input type="text" class="form-control" id="team-right-name" name="team-right-name" rv-value="teams.right.name" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="team-right-abbreviation">Abbreviation</label>
                                        <input type="text" class="form-control" id="team-right-abbreviation" name="team-right-abbreviation" rv-value="teams.right.abbreviation" placeholder="Abbreviation">
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            Roster
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="team-right-player1">Player 1</label>
                                                        <input type="text" class="form-control" id="team-right-player1" name="team-right-player1" rv-value="teams.right.players.one.name" placeholder="Player 1">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="team-right-player2">Player 2</label>
                                                        <input type="text" class="form-control" id="team-right-player2" name="team-right-player2" rv-value="teams.right.players.two.name" placeholder="Player 2">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="team-right-player3">Player 3</label>
                                                        <input type="text" class="form-control" id="team-right-player3" name="team-right-player3" rv-value="teams.right.players.three.name" placeholder="Player 3">
                                                    </div>  
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary float-right" rv-on-click="upNext.handleCommit">Commit</button>
                </div>
            </div>
        `;
    }

    entry() {
    }

    handleCommitUpUpTeams() {
        let teams = _.cloneDeep(this.binds.teams);
        WsOverlayServer.send("sos", "up_next_teams_update", teams);
        WsOverlayServer.send("local", "UpNext.Teams", teams);
        vMix.updateUpNextTeams(teams);
    }
}
$(function () {
    TabsController.register(UpNextTab);
});