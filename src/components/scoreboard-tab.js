class ScorebugTab {
    id = "Scorebug";
    headerText = "Scorebug";
    sortPriority = 1;

    binds = {
        _this: this,
        popunderTeamOptions: [],
        popunderText: "",
        teams: {
            left: {
                name: "",
                abbreviation: "",
                score: 0,
            },
            right: {
                name: "",
                abbreviation: "",
                score: 0,
            }
        },
        gameNumber: 0,
        winCondition: 0,
        enableAutoplayMusic: false,
        commitHandler: function (e, b) {
            b._this.scoreboardCommitHandler();
        },
        showSeriesScoreEventHandler: function (e, b) {
            let customMessage = "";

            let tie = b.teams.left.score === b.teams.right.score;
            if (tie) {
                customMessage = `SERIES <b>TIED ${b.teams.left.score}-${b.teams.right.score}</b>`;
            } else {
                let leadTeam = b.teams.left.score >= b.teams.right.score ? 'left' : 'right';
                let losingTeam = leadTeam === 'left' ? 'right' : 'left';
                customMessage = `<b>${b.teams[leadTeam].abbreviation}</b> leads <b>${b.teams[leadTeam].score}-${b.teams[losingTeam].score}</b>`;
            }

            WsOverlayServer.send("sos", "series_score_update_text", {text: customMessage});
        },
        handleShowPopunderConfirm: function (e, b) {
            let team = formDataGetByValue("popunder_team_options", $("#scorebug-popunder-options option:selected").val());
            if (_.isUndefined(team)) {
                ModalController.show("Error", "Team must be selected for popunder");
                return;
            }
            ModalController.show({
                title: "Confirm Popunder",
                message: `Team: ${team.display}<br>
                          Message: ${b.popunderText}`,
                size: ModalController.sizes.Large,
                buttons: ModalController.buttonOptions.ContinueCancel
            }).then(function () {
                WsOverlayServer.send("sos", "popunder_show", {
                    team: team.value,
                    message: b.popunderText
                });
            });
        }
    };

    template() {
        return `
            <div id="caster-controls">
                <div class="card">
                    <div class="card-header">
                        Scorebug Controls
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="card">
                                    <div class="card-header">
                                        Left Team (Blue)
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="team-left-name">Name</label>
                                            <input type="text" class="form-control" id="team-left-name" name="team-left-name" rv-value="teams.left.name" placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="team-left-abbreviation">Abbreviation</label>
                                            <input type="text" class="form-control" id="team-left-abbreviation" name="team-left-abbreviation" rv-value="teams.left.abbreviation" placeholder="Abbreviation">
                                        </div>
                                        <div class="form-group">
                                            <label for="team-left-score">Score</label>
                                            <input type="number" class="form-control" id="team-left-score" name="team-left-score" rv-value="teams.left.score" placeholder="Score">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card">
                                    <div class="card-header">
                                        Right Team (Orange)
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="team-right-name">Name</label>
                                            <input type="text" class="form-control" id="team-right-name" name="team-right-name" rv-value="teams.right.name" placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="team-right-abbreviation">Abbreviation</label>
                                            <input type="text" class="form-control" id="team-right-abbreviation" name="team-right-abbreviation" rv-value="teams.right.abbreviation" placeholder="Abbreviation">
                                        </div>
                                        <div class="form-group">
                                            <label for="team-right-score">Score</label>
                                            <input type="number" class="form-control" id="team-right-score" name="team-right-score" rv-value="teams.right.score" placeholder="Score">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="scorebug-win-condition">Win Condition (Bo5 = 3, Bo7 = 4)</label>
                                    <input type="number" class="form-control" id="scorebug-win-condition" name="scorebug-win-condition" rv-value="winCondition" placeholder="Win Condition">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="scorebug-game-number">Game Number</label>
                                    <input type="number" class="form-control" id="scorebug-game-number" name="scorebug-game-number" rv-value="gameNumber" placeholder="Game Number">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button class="btn btn-primary float-right" rv-on-click="commitHandler">Commit</button>
                                <button class="btn btn-danger float-right" rv-on-click="showSeriesScoreEventHandler">Show Series Score Event</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="scorebug-popunder-text">Popunder Text</label>
                                    <input type="text" class="form-control" id="scorebug-popunder-text" name="scorebug-popunder-text" rv-value="popunderText" placeholder="Popunder Text">
                                </div>  
                            </div>
                            <div class="col">
                                  <label for="scorebug-popunder-options" class="label">Popunder Team</label>
                                  <select name="scorebug-popunder-options" id="scorebug-popunder-options" class="custom-select">
                                      <option selected disabled>Popunder Team</option>
                                      <option rv-each-option="popunderTeamOptions" rv-value="option.value">{option.display}</option>
                                  </select>                      
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button class="btn btn-danger" rv-on-click="handleShowPopunderConfirm">Show Scorebug Popunder</button>  
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col">
                                <div class="form-check">
                                    <input class="form-check-input" name="autoplay-music-enable" type="checkbox" rv-checked="enableAutoplayMusic">
                                    <label class="form-check-label" for="autoplay-music-enable">
                                    Enable End Of Game Music (Autoplay)
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }

    players = {};
    playerGoalsAssits = {};
    firstCountdownHit = false;
    eventCooldowns = {};
    entry() {
        let _this = this;
        form_data['popunder_team_options'] = [
            {value:"left", display: "Left (Blue)"},
            {value:"right", display: "Right (Orange)"},
        ];
        this.binds.popunderTeamOptions = form_data['popunder_team_options'];

        WsOverlayServer.subscribe('game', 'match_created', (d) => {
            this.firstCountdownHit = false;
            vMix.triggerHideScorebug();
        });
        WsOverlayServer.subscribe('game', 'post_countdown_begin', (d) => {
            if (!this.firstCountdownHit) {
                this.players = {};
                this.firstCountdownHit = true;
                setTimeout(function () {
                    vMix.triggerScorebugReveal();
                }, 4000);
            }
        });
        WsOverlayServer.subscribe('game', 'update_state', function (d) {
            //players state update
            _this.players = _.merge(_this.players, _.keyBy(Object.values(d['players']), 'name'));
            vMix.updatePostGameStatsLeaderboard(
                {
                    left: {
                        name: _this.binds.teams.left.name,
                        abbreviation: _this.binds.teams.left.abbreviation,
                        score: _.sumBy(_.toArray(_.filter(_this.players, {team: 0})), 'goals'),
                    },
                    right: {
                        name: _this.binds.teams.right.name,
                        abbreviation: _this.binds.teams.right.abbreviation,
                        score: _.sumBy(_.toArray(_.filter(_this.players, {team: 1})), 'goals'),
                    }
                },
                _.reverse(_.sortBy(_.toArray(_.filter(_this.players, {"team": 0})), "score")),
                _.reverse(_.sortBy(_.toArray(_.filter(_this.players, {"team": 1})), "score"))
            );
        });

        let message = "";
        let debouncedPopunderGoal = _.debounce(function (team, message) {
            WsOverlayServer.send("sos", "popunder_show", {
                team: team,
                message: message
            });
        }, 300);
        WsOverlayServer.subscribe("game", "statfeed_event", d => {
            let cdCheck = d.type + d.main_target;
            if (this.eventCooldowns.hasOwnProperty(cdCheck)) {
                if ((+new Date()) - this.eventCooldowns[cdCheck] < 50) {
                    return;
                }
            }
            this.eventCooldowns[cdCheck] = +new Date();

            if (d.type === 'Goal') {
                message = "";
                let player = _.find(Object.values(_this.players), {'name': d.main_target});
                if (!_.isUndefined(player)) {
                    vMix.teamGoalAnimation(player.team === 0 ? 'left' : 'right');
                    message += `GOAL: ${player.name} (${player.goals+1})`;
                    debouncedPopunderGoal(player.team === 0 ? 'left' : 'right', message);
                }
            } else if (d.type === 'Assist') {
                let player = _.find(Object.values(_this.players), {'name': d.main_target});
                if (!_.isUndefined(player)) {
                    message += ` - ASSIST: ${player.name} (${player.assists+1})`;
                    debouncedPopunderGoal(player.team === 0 ? 'left' : 'right', message);
                }
            }
        });

        WsOverlayServer.subscribe("game", "match_ended", function (d) {
            let team = "left";
            if (d['winner_team_num'] === 1) {
                team = "right";
            }

            _this.binds["teams"][team].score++;
            if (_this.binds.winCondition > _this.binds.gameNumber) {
                _this.binds.gameNumber++;
            }
            _this.scoreboardCommitHandler();

            vMix.updatePostGameStatsLeaderboard({
                left: {
                    name: _this.binds.teams.left.name,
                    abbreviation: _this.binds.teams.left.abbreviation,
                    score: _this.binds.teams.left.score,
                },
                right: {
                    name: _this.binds.teams.right.name,
                    abbreviation: _this.binds.teams.right.abbreviation,
                    score: _this.binds.teams.right.score,
                }
            });

            if (_this.binds.enableAutoplayMusic) {
                vMix.triggerPlayEndMusic();
                if (_this.binds.teams.left.score >= _this.binds.winCondition || _this.binds.teams.right.score >= _this.binds.winCondition) {
                    vMix.triggerEndOfSeriesVideo();
                    _this.binds.enableAutoplayMusic = false;
                } else {
                    setTimeout(function () {
                        vMix.triggerEndOfMatchTransition();
                    }, 6000);
                }
            }

            _this.binds.showSeriesScoreEventHandler(undefined, _this.binds);
        });

        WsOverlayServer.subscribe("game", "replay_start", () => {
            vMix.triggerEnterReplay();
        });
        WsOverlayServer.subscribe("game", "replay_end", () => {
            vMix.triggerExitReplay();
        });
    }

    lastCommitNameLeft = "";
    lastCommitNameRight = "";
    scoreboardCommitHandler() {
        if (this.binds.teams.left.name !== this.lastCommitNameLeft || this.binds.teams.right.name !== this.lastCommitNameRight) {
            ModalController.show({
                title: "Teams Changed",
                message: `The teams have changed. Make sure to update the following<br>
                          <ul>
                              <li>Post Game Stats - Team Logos</li>
                          </ul>`,
                size: ModalController.sizes.Large
            });
        }

        this.lastCommitNameLeft = this.binds.teams.left.name;
        this.lastCommitNameRight = this.binds.teams.right.name;
        WsOverlayServer.send("sos", "scorebug_teams_update", {
            teams: {
                left: {
                    name: this.binds.teams.left.name,
                    abbreviation: this.binds.teams.left.abbreviation,
                    score: this.binds.teams.left.score,
                },
                right: {
                    name: this.binds.teams.right.name,
                    abbreviation: this.binds.teams.right.abbreviation,
                    score: this.binds.teams.right.score,
                }
            },
            gameNumber: this.binds.gameNumber
        });
    }
}
$(function () {
    TabsController.register(ScorebugTab);
});