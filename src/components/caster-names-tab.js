class CasterNamesTab {
    id = "CasterNames";
    headerText = "Caster Names";

    binds = {
        casters: {
            left: {
                name: "",
                handle: "",
            },
            right: {
                name: "",
                handle: "",
            },
            three: {
                name: "",
                handle: "",
            },
            four: {
                name: "",
                handle: "",
            },
            handleCommit: function (e, b) {
                vMix.runCommand(`Function=SetText&Input=CasterWindow+Left&SelectedName=Caster-Name.Text&Value=${b.casters.left.name}`);
                vMix.runCommand(`Function=SetText&Input=CasterWindow+Left&SelectedName=Caster-Handle.Text&Value=${b.casters.left.handle}`);

                vMix.runCommand(`Function=SetText&Input=CasterWindow+Right&SelectedName=Caster-Name.Text&Value=${b.casters.right.name}`);
                vMix.runCommand(`Function=SetText&Input=CasterWindow+Right&SelectedName=Caster-Handle.Text&Value=${b.casters.right.handle}`);

                vMix.runCommand(`Function=SetText&Input=CasterWindow+Three&SelectedName=Caster-Name.Text&Value=${b.casters.three.name}`);
                vMix.runCommand(`Function=SetText&Input=CasterWindow+Three&SelectedName=Caster-Handle.Text&Value=${b.casters.three.handle}`);

                vMix.runCommand(`Function=SetText&Input=CasterWindow+Four&SelectedName=Caster-Name.Text&Value=${b.casters.four.name}`);
                vMix.runCommand(`Function=SetText&Input=CasterWindow+Four&SelectedName=Caster-Handle.Text&Value=${b.casters.four.handle}`);
            }
        }
    };

    template() {
        return `
            <div class="card">
                <div class="card-header">
                    Caster Names Controls    
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="caster-left-name">Caster Left Name</label>
                                        <input type="text" class="form-control" id="caster-left-name" name="caster-left-name" rv-value="casters.left.name" placeholder="Caster Left Name">
                                    </div>  
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="caster-left-handle">Caster Left Handle</label>
                                        <input type="text" class="form-control" id="caster-left-handle" name="caster-left-handle" rv-value="casters.left.handle" placeholder="Caster Left Handle">
                                    </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="caster-right-name">Caster Right Name</label>
                                        <input type="text" class="form-control" id="caster-right-name" name="caster-right-name" rv-value="casters.right.name" placeholder="Caster Right Name">
                                    </div>  
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="caster-right-handle">Caster Right Handle</label>
                                        <input type="text" class="form-control" id="caster-right-handle" name="caster-right-handle" rv-value="casters.right.handle" placeholder="Caster Right Handle">
                                    </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="caster-three-name">Caster Three Name</label>
                                        <input type="text" class="form-control" id="caster-three-name" name="caster-three-name" rv-value="casters.three.name" placeholder="Caster Three Name">
                                    </div>  
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="caster-three-handle">Caster Three Handle</label>
                                        <input type="text" class="form-control" id="caster-three-handle" name="caster-three-handle" rv-value="casters.three.handle" placeholder="Caster Three Handle">
                                    </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="caster-four-name">Caster Four Name</label>
                                        <input type="text" class="form-control" id="caster-four-name" name="caster-four-name" rv-value="casters.four.name" placeholder="Caster Four Name">
                                    </div>  
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="caster-four-handle">Caster Four Handle</label>
                                        <input type="text" class="form-control" id="caster-four-handle" name="caster-four-handle" rv-value="casters.four.handle" placeholder="Caster Four Handle">
                                    </div>  
                                </div>
                            </div>
                            <button class="btn btn-primary float-right" rv-on-click="casters.handleCommit">Commit</button>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }

    entry() {
    }
}
$(function () {
    TabsController.register(CasterNamesTab);
});