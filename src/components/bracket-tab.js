class BracketTab {
    id = "Bracket";
    headerText = "Bracket";

    binds = {
        _this: this,
        bracket: {
            handleCommit: function (e, b) {
                b._this.handleBracketCommit();
            },
            qf: {
                groups: {
                    one: {
                        top: {
                            name: "",
                            seed: "",
                            score: "",
                        },
                        bottom: {
                            name: "",
                            seed: "",
                            score: "",
                        }
                    },
                    two: {
                        top: {
                            name: "",
                            seed: "",
                            score: "",
                        },
                        bottom: {
                            name: "",
                            seed: "",
                            score: "",
                        }
                    },
                    three: {
                        top: {
                            name: "",
                            seed: "",
                            score: "",
                        },
                        bottom: {
                            name: "",
                            seed: "",
                            score: "",
                        }
                    },
                    four: {
                        top: {
                            name: "",
                            seed: "",
                            score: "",
                        },
                        bottom: {
                            name: "",
                            seed: "",
                            score: "",
                        }
                    },
                }
            },
            sf: {
                groups: {
                    one: {
                        top: {
                            name: "",
                            seed: "",
                            score: "",
                        },
                        bottom: {
                            name: "",
                            seed: "",
                            score: "",
                        }
                    },
                    two: {
                        top: {
                            name: "",
                            seed: "",
                            score: "",
                        },
                        bottom: {
                            name: "",
                            seed: "",
                            score: "",
                        }
                    },
                }
            },
            ff: {
                groups: {
                    one: {
                        top: {
                            name: "",
                            seed: "",
                            score: "",
                        },
                        bottom: {
                            name: "",
                            seed: "",
                            score: "",
                        }
                    },
                }
            }
        }
    };

    template() {
        return `
            <div class="card">
                <div class="card-header">
                    Bracket Controls
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    Quarter Finals
                                </div>
                                <div class="card-body">
                                    <div class="card">
                                        <div class="card-header">
                                            1st Group
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-one-top-name">Top Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-one-top-name" name="bracket-qf-groups-one-top-name" rv-value="bracket.qf.groups.one.top.name" placeholder="Top Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-one-top-seed">Top Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-one-top-seed" name="bracket-qf-groups-one-top-seed" rv-value="bracket.qf.groups.one.top.seed" placeholder="Top Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-one-top-score">Top Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-one-top-score" name="bracket-qf-groups-one-top-score" rv-value="bracket.qf.groups.one.top.score" placeholder="Top Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-one-bottom-name">Bottom Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-one-bottom-name" name="bracket-qf-groups-one-bottom-name" rv-value="bracket.qf.groups.one.bottom.name" placeholder="Bottom Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-one-bottom-seed">Bottom Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-one-bottom-seed" name="bracket-qf-groups-one-bottom-seed" rv-value="bracket.qf.groups.one.bottom.seed" placeholder="Bottom Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-one-bottom-score">Bottom Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-one-bottom-score" name="bracket-qf-groups-one-bottom-score" rv-value="bracket.qf.groups.one.bottom.score" placeholder="Bottom Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            2nd Group
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-two-top-name">Top Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-two-top-name" name="bracket-qf-groups-two-top-name" rv-value="bracket.qf.groups.two.top.name" placeholder="Top Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-two-top-seed">Top Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-two-top-seed" name="bracket-qf-groups-two-top-seed" rv-value="bracket.qf.groups.two.top.seed" placeholder="Top Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-two-top-score">Top Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-two-top-score" name="bracket-qf-groups-two-top-score" rv-value="bracket.qf.groups.two.top.score" placeholder="Top Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-two-bottom-name">Bottom Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-two-bottom-name" name="bracket-qf-groups-two-bottom-name" rv-value="bracket.qf.groups.two.bottom.name" placeholder="Bottom Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-two-bottom-seed">Bottom Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-two-bottom-seed" name="bracket-qf-groups-two-bottom-seed" rv-value="bracket.qf.groups.two.bottom.seed" placeholder="Bottom Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-two-bottom-score">Bottom Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-two-bottom-score" name="bracket-qf-groups-two-bottom-score" rv-value="bracket.qf.groups.two.bottom.score" placeholder="Bottom Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            3rd Group
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-three-top-name">Top Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-three-top-name" name="bracket-qf-groups-three-top-name" rv-value="bracket.qf.groups.three.top.name" placeholder="Top Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-three-top-seed">Top Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-three-top-seed" name="bracket-qf-groups-three-top-seed" rv-value="bracket.qf.groups.three.top.seed" placeholder="Top Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-three-top-score">Top Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-three-top-score" name="bracket-qf-groups-three-top-score" rv-value="bracket.qf.groups.three.top.score" placeholder="Top Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-three-bottom-name">Bottom Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-three-bottom-name" name="bracket-qf-groups-three-bottom-name" rv-value="bracket.qf.groups.three.bottom.name" placeholder="Bottom Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-three-bottom-seed">Bottom Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-three-bottom-seed" name="bracket-qf-groups-three-bottom-seed" rv-value="bracket.qf.groups.three.bottom.seed" placeholder="Bottom Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-three-bottom-score">Bottom Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-three-bottom-score" name="bracket-qf-groups-three-bottom-score" rv-value="bracket.qf.groups.three.bottom.score" placeholder="Bottom Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            4th Group
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-four-top-name">Top Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-four-top-name" name="bracket-qf-groups-four-top-name" rv-value="bracket.qf.groups.four.top.name" placeholder="Top Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-four-top-seed">Top Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-four-top-seed" name="bracket-qf-groups-four-top-seed" rv-value="bracket.qf.groups.four.top.seed" placeholder="Top Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-four-top-score">Top Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-four-top-score" name="bracket-qf-groups-four-top-score" rv-value="bracket.qf.groups.four.top.score" placeholder="Top Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-four-bottom-name">Bottom Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-four-bottom-name" name="bracket-qf-groups-four-bottom-name" rv-value="bracket.qf.groups.four.bottom.name" placeholder="Bottom Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-four-bottom-seed">Bottom Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-four-bottom-seed" name="bracket-qf-groups-four-bottom-seed" rv-value="bracket.qf.groups.four.bottom.seed" placeholder="Bottom Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-qf-groups-four-bottom-score">Bottom Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-qf-groups-four-bottom-score" name="bracket-qf-groups-four-bottom-score" rv-value="bracket.qf.groups.four.bottom.score" placeholder="Bottom Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    Semi Finals
                                </div>
                                <div class="card-body">
                                    <div class="card">
                                        <div class="card-header">
                                            1st Group
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-sf-groups-one-top-name">Top Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-sf-groups-one-top-name" name="bracket-sf-groups-one-top-name" rv-value="bracket.sf.groups.one.top.name" placeholder="Top Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-sf-groups-one-top-seed">Top Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-sf-groups-one-top-seed" name="bracket-sf-groups-one-top-seed" rv-value="bracket.sf.groups.one.top.seed" placeholder="Top Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-sf-groups-one-top-score">Top Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-sf-groups-one-top-score" name="bracket-sf-groups-one-top-score" rv-value="bracket.sf.groups.one.top.score" placeholder="Top Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-sf-groups-one-bottom-name">Bottom Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-sf-groups-one-bottom-name" name="bracket-sf-groups-one-bottom-name" rv-value="bracket.sf.groups.one.bottom.name" placeholder="Bottom Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-sf-groups-one-bottom-seed">Bottom Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-sf-groups-one-bottom-seed" name="bracket-sf-groups-one-bottom-seed" rv-value="bracket.sf.groups.one.bottom.seed" placeholder="Bottom Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-sf-groups-one-bottom-score">Bottom Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-sf-groups-one-bottom-score" name="bracket-sf-groups-one-bottom-score" rv-value="bracket.sf.groups.one.bottom.score" placeholder="Bottom Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            2nd Group
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-sf-groups-two-top-name">Top Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-sf-groups-two-top-name" name="bracket-sf-groups-two-top-name" rv-value="bracket.sf.groups.two.top.name" placeholder="Top Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-sf-groups-two-top-seed">Top Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-sf-groups-two-top-seed" name="bracket-sf-groups-two-top-seed" rv-value="bracket.sf.groups.two.top.seed" placeholder="Top Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-sf-groups-two-top-score">Top Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-sf-groups-two-top-score" name="bracket-sf-groups-two-top-score" rv-value="bracket.sf.groups.two.top.score" placeholder="Top Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-sf-groups-two-bottom-name">Bottom Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-sf-groups-two-bottom-name" name="bracket-sf-groups-two-bottom-name" rv-value="bracket.sf.groups.two.bottom.name" placeholder="Bottom Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-sf-groups-two-bottom-seed">Bottom Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-sf-groups-two-bottom-seed" name="bracket-sf-groups-two-bottom-seed" rv-value="bracket.sf.groups.two.bottom.seed" placeholder="Bottom Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-sf-groups-two-bottom-score">Bottom Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-sf-groups-two-bottom-score" name="bracket-sf-groups-two-bottom-score" rv-value="bracket.sf.groups.two.bottom.score" placeholder="Bottom Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    Finals
                                </div>
                                <div class="card-body">
                                    
                                    <div class="card">
                                        <div class="card-header">
                                            1st Group
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-ff-groups-one-top-name">Top Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-ff-groups-one-top-name" name="bracket-ff-groups-one-top-name" rv-value="bracket.ff.groups.one.top.name" placeholder="Top Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-ff-groups-one-top-seed">Top Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-ff-groups-one-top-seed" name="bracket-ff-groups-one-top-seed" rv-value="bracket.ff.groups.one.top.seed" placeholder="Top Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-ff-groups-one-top-score">Top Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-ff-groups-one-top-score" name="bracket-ff-groups-one-top-score" rv-value="bracket.ff.groups.one.top.score" placeholder="Top Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-ff-groups-one-bottom-name">Bottom Team Name</label>
                                                        <input type="text" class="form-control" id="bracket-ff-groups-one-bottom-name" name="bracket-ff-groups-one-bottom-name" rv-value="bracket.ff.groups.one.bottom.name" placeholder="Bottom Team">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-ff-groups-one-bottom-seed">Bottom Team Seed</label>
                                                        <input type="text" class="form-control" id="bracket-ff-groups-one-bottom-seed" name="bracket-ff-groups-one-bottom-seed" rv-value="bracket.ff.groups.one.bottom.seed" placeholder="Bottom Team Seed">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="bracket-ff-groups-one-bottom-score">Bottom Team Score</label>
                                                        <input type="text" class="form-control" id="bracket-ff-groups-one-bottom-score" name="bracket-ff-groups-one-bottom-score" rv-value="bracket.ff.groups.one.bottom.score" placeholder="Bottom Team Score">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary float-right" rv-on-click="bracket.handleCommit">Commit</button>
                </div>
            </div>
        `;
    }

    entry() {
    }

    handleBracketCommit() {
        WsOverlayServer.send("sos", "bracket_update", {
            qf: _.cloneDeep(this.binds.bracket.qf),
            sf: _.cloneDeep(this.binds.bracket.sf),
            ff: _.cloneDeep(this.binds.bracket.ff),
        });
    }
}
$(function () {
    TabsController.register(BracketTab);
});