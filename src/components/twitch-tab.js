class TwitchTab {
    id = "twitch";
    headerText = "Twitch";

    binds = {
        _this: this,
        voteCommandLeft: "",
        voteCommandRight: "",
        votables: [],
        votingActive: false,
        startVotingHandler: function (e,b) {
            if (!b.votingActive) {
                let body = `<div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="twitch-command-left">Left Vote Command</label>
                                        <input type="text" class="form-control" id="twitch-command-left" name="twitch-command-left" value="${b.voteCommandLeft}" placeholder="Left Vote Command">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="twitch-command-right">Right Vote Command</label>
                                        <input type="text" class="form-control" id="twitch-command-right" name="twitch-command-right" value="${b.voteCommandRight}" placeholder="Right Vote Command">
                                    </div>
                                </div>
                            </div>`;
                ModalController.show({
                    title: "Start Voting",
                    message: body,
                    size: ModalController.sizes.Large,
                    type: ModalController.types.CustomForm,
                    buttons: ModalController.buttonOptions.ContinueCancel
                })
                    .then((commandResponse) => {
                        let leftCommand = commandResponse['twitch-command-left'].value;
                        let rightCommand = commandResponse['twitch-command-right'].value;

                        if (leftCommand.length < 1 || rightCommand.length < 1) {
                            ModalController.show("Command Entry Error", "Commands must be entered to start voting");
                        } else {
                            b.votables = [
                                {
                                    side: "left",
                                    command: leftCommand.toUpperCase(),
                                    total: 0,
                                    percent: "0%"
                                },
                                {
                                    side: "right",
                                    command: rightCommand.toUpperCase(),
                                    total: 0,
                                    percent: "0%"
                                },
                            ];
                            b._this.currentVotes = {};
                            b.votingActive = b._this.votingActive = true;
                            b._this.voteDataCompilerInterval = setInterval(function () {
                                let totalVotes = 0;
                                let voteCounts = {};
                                //prepass for total votes
                                _.each(b.votables, (v, i) => {
                                    let votes = _.filter(b._this.currentVotes, {message: v.command})
                                    _.each(votes, function (v, k) {
                                        if (b._this.currentVotes.hasOwnProperty(v.sender) && b._this.currentVotes[v.sender]['locked'] !== true) {
                                            b._this.currentVotes[v.sender]['locked'] = true;
                                        }
                                    });
                                    totalVotes += votes.length;
                                });
                                _.each(b.votables, (v, i) => {
                                    let votes = _.filter(b._this.currentVotes, {message: v.command}).length;
                                    let percent = totalVotes === 0 ? 0.5 : _.clamp(_.defaultTo(_.round(votes / totalVotes, 2), 0), 0, 1);
                                    voteCounts[v.side] = {
                                        command: v.command,
                                        total: votes,
                                        percent: percent,
                                    };
                                    b.votables[i].total = votes;
                                    b.votables[i].percent = (_.round(percent * 100)).toString().substr(0, percent === 1 ? 3 : 2) + "%";
                                });
                                WsOverlayServer.send("sos", "twitch_votes_update", voteCounts);
                            }, 1000);
                        }
                    });
            }
        },
        stopVotingHandler: function (e, b) {
            ModalController.show({
                title: "Stop Voting",
                message: "Are you sure you wish to stop voting? Voting cannot be resumed",
                size: ModalController.sizes.Large,
                buttons: ModalController.buttonOptions.YesNo
            }).then(() => {
                b.votingActive = b._this.votingActive = false;
                clearInterval(b._this.voteDataCompilerInterval);
            });
        },
        clearVotablesHandler: function (e, b) {
            if (b.votingActive) {
                ModalController.show({
                    title: "Active Voting",
                    message: "Cannot clear votables while votes are active"
                });
            } else {
                b.votables = [];
                b._this.currentVotes = {};
            }
        }
    };

    votingActive = false;
    currentVotes = {}; // Keyed by sender name
    voteDataCompilerInterval;

    template() {
        return `<div class="card">
                    <div class="card-header">
                        Caster Controls
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-bordered">
                            <tr>
                                <th>Vote Command</th>
                                <th>Total</th>
                                <th>Percent</th>
                            </tr>
                            <tr rv-each-votable="votables">
                                <td>{votable.command}</td>
                                <td>{votable.total}</td>
                                <td>{votable.percent}</td>
                            </tr>
                            <tr rv-hide="votables.length"><!-- Kinda hacky, but the RV props make this array eval to true even when empty -->
                                <td class="text-center" colspan="3">Nothing Available To Vote On</td>
                            </tr>
                        </table>
                        <div class="row">
                            <div class="col">
                                <button class="btn btn-danger hr-margin-5" rv-disabled="votingActive" rv-on-click="startVotingHandler">Start Voting</button>
                                <button class="btn btn-danger hr-margin-5" rv-enabled="votingActive" rv-on-click="stopVotingHandler">Stop Voting</button>
                                <button class="btn btn-danger hr-margin-5" rv-disabled="votingActive" rv-on-click="clearVotablesHandler">Clear Votables</button>
                            </div>
                        </div>
                    </div>
                </div>`;
    }

    upNextTeams = {};
    entry() {
        let _this = this;
        WsOverlayServer.subscribe("twitch", "message", (data) => {
            if (this.votingActive) {
                if (this.currentVotes.hasOwnProperty(data.sender) && this.currentVotes[data.sender].locked === true) {
                    console.log("Already voted");
                    return;
                }
                data['message'] = data['message'].toUpperCase();
                this.currentVotes[data.sender] = data;
            }
        });
        WsOverlayServer.subscribe('local', 'UpNext.Teams', d => {
            this.upNextTeams = d;
            this.binds.voteCommandLeft = "!"+d.left.abbreviation;
            this.binds.voteCommandRight = "!"+d.right.abbreviation;
        });
    }
}

$(function () {
    TabsController.register(TwitchTab);
});