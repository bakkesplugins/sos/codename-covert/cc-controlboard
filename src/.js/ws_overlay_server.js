const WsOverlayServer = {
    __subscribers: {},
    websocket: undefined,
    webSocketConnected: false,
    registerQueue: [],
    init: function(host, port, debug, debugFilters) {
        host = host || 'localhost';
        port = port || 49322;
        debug = debug || false;
        if (debug) {
            if (debugFilters !== undefined) {
                console.warn("WebSocket Debug Mode enabled with filtering. Only events not in the filter list will be dumped");
            } else {
                console.warn("WebSocket Debug Mode enabled without filters applied. All events will be dumped to console");
                console.warn("To use filters, pass in an array of 'channel:event' strings to the second parameter of the init function");
            }
        }
        WsOverlayServer.webSocket = new WebSocket(`ws://${host}:${port}`);
        WsOverlayServer.webSocket.onmessage = function (event) {
            let jEvent = JSON.parse(event.data);
            if (!jEvent.hasOwnProperty('event')) {
                return;
            }
            let eventSplit = jEvent.event.split(':');
            let channel = eventSplit[0];
            let event_event = eventSplit[1];
            if (debug) {
                if (!debugFilters) {
                    console.log(channel, event_event, jEvent);
                } else if (debugFilters && debugFilters.indexOf(jEvent.event) < 0) {
                    console.log(channel, event_event, jEvent);
                }
            }
            WsOverlayServer.triggerSubscribers(channel, event_event, jEvent.data);
        };
        WsOverlayServer.webSocket.onopen = function () {
            WsOverlayServer.triggerSubscribers("ws", "open");
            WsOverlayServer.webSocketConnected = true;
            _.each(WsOverlayServer.registerQueue, (r) => {
                WsOverlayServer.send("wsRelay", "register", r);
            });
        };
        WsOverlayServer.webSocket.onerror = function () {
            WsOverlayServer.triggerSubscribers("ws", "error");
            WsOverlayServer.webSocketConnected = false;
        };
        WsOverlayServer.webSocket.onclose = function () {
            WsOverlayServer.triggerSubscribers("ws", "close");
            WsOverlayServer.webSocketConnected = false;
        };
    },
    /**
     * Add callbacks for when certain events are thrown
     * Execution is guaranteed to be in First In First Out order
     * @param channels
     * @param events
     * @param callback
     */
    subscribe: function(channels, events, callback) {
        if (_.isString(channels)) {
            let channel = channels;
            channels = [];
            channels.push(channel);
        }
        if (_.isString(events)) {
            let event = events;
            events = [];
            events.push(event);
        }
        channels.forEach(function(c) {
            events.forEach(function (e) {
                if (!WsOverlayServer.__subscribers.hasOwnProperty(c)) {
                    WsOverlayServer.__subscribers[c] = {};
                }
                if (!WsOverlayServer.__subscribers[c].hasOwnProperty(e)) {
                    WsOverlayServer.__subscribers[c][e] = [];
                    if (WsOverlayServer.webSocketConnected) {
                        WsOverlayServer.send("wsRelay", "register", `${c}:${e}`);
                    } else {
                        WsOverlayServer.registerQueue.push(`${c}:${e}`);
                    }
                }
                WsOverlayServer.__subscribers[c][e].push(callback);
            });
        })
    },
    clearEventCallbacks: function (channel, event) {
        if (WsOverlayServer.__subscribers.hasOwnProperty(channel) && WsOverlayServer.__subscribers[channel].hasOwnProperty(event)) {
            WsOverlayServer.__subscribers[channel] = {};
        }
    },
    triggerSubscribers: function (channel, event, data) {
        if (WsOverlayServer.__subscribers.hasOwnProperty(channel) && WsOverlayServer.__subscribers[channel].hasOwnProperty(event)) {
            WsOverlayServer.__subscribers[channel][event].forEach(function(callback) {
                if (_.isFunction(callback)) {
                    callback(data);
                }
            });
        }
    },
    send: function (channel, event, data) {
        if (typeof channel !== 'string') {
            console.error("Channel must be a string");
            return;
        }
        if (typeof event !== 'string') {
            console.error("Event must be a string");
            return;
        }
        if (channel === 'local') {
            this.triggerSubscribers(channel, event, data);
        } else {
            let cEvent = channel + ":" + event;
            WsOverlayServer.webSocket.send(JSON.stringify({
                'event': cEvent,
                'data': data
            }));
        }
    }
};