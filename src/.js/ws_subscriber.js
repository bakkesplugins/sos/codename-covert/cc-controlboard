const WsRocketLeague = {
    __subscribers: {},
    /**
     * Initialize the web socket and event messenger
     * @param port Int Port number to connect to websocket with
     * @param debug Bool Enable debug mode
     * @param debugFilters Array Events to filter out of console logging if debug is enabled
     */
    init: function(host, port, debug, debugFilters) {
        host = host || 'localhost';
        port = port || 49122;
        debug = debug || false;
        if (debug) {
            if (debugFilters !== undefined) {
                console.warn("WebSocket Debug Mode enabled with filtering. Only events not in the filter list will be dumped");
            } else {
                console.warn("WebSocket Debug Mode enabled without filters applied. All events will be dumped to console");
                console.warn("To use filters, pass in an array of 'channel:event' strings to the second parameter of the init function");
            }
        }
        let webSocket = new WebSocket(`ws://${host}:${port}`);
        webSocket.onmessage = function (event) {
            let jEvent = JSON.parse(event.data);
            if (!jEvent.hasOwnProperty('event')) {
                return;
            }
            let eventSplit = jEvent.event.split(':');
            let channel = eventSplit[0];
            let event_event = eventSplit[1];
            if (debug) {
                if (debugFilters && debugFilters.indexOf(jEvent.event) < 0) {
                    console.log(channel, event_event, jEvent);
                }
            }
            WsRocketLeague.triggerSubscribers(channel, event_event, jEvent.data);
            if (WsOverlayServer.webSocketConnected) {
                WsOverlayServer.send(channel, event_event, jEvent.data);
            }
        };
        webSocket.onopen = function () {
            WsRocketLeague.triggerSubscribers("ws", "open");
        };
        webSocket.onerror = function () {
            WsRocketLeague.triggerSubscribers("ws", "error");
        };
        webSocket.onclose = function () {
            WsRocketLeague.triggerSubscribers("ws", "close");
        };
    },
    /**
     * Add callbacks for when certain events are thrown
     * Execution is guaranteed to be in First In First Out order
     * @param channels
     * @param events
     * @param callback
     */
    subscribe: function(channels, events, callback) {
        if (_.isString(channels)) {
            let channel = channels;
            channels = [];
            channels.push(channel);
        }
        if (_.isString(events)) {
            let event = events;
            events = [];
            events.push(event);
        }
        channels.forEach(function(c) {
            events.forEach(function (e) {
                if (!WsRocketLeague.__subscribers.hasOwnProperty(c)) {
                    WsRocketLeague.__subscribers[c] = {};
                }
                if (!WsRocketLeague.__subscribers[c].hasOwnProperty(e)) {
                    WsRocketLeague.__subscribers[c][e] = [];
                }
                WsRocketLeague.__subscribers[c][e].push(callback);
            });
        })
    },
    clearEventCallbacks: function (channel, event) {
        if (WsRocketLeague.__subscribers.hasOwnProperty(channel) && WsRocketLeague.__subscribers[channel].hasOwnProperty(event)) {
            WsRocketLeague.__subscribers[channel] = {};
        }
    },
    triggerSubscribers: function (channel, event, data) {
        if (WsRocketLeague.__subscribers.hasOwnProperty(channel) && WsRocketLeague.__subscribers[channel].hasOwnProperty(event)) {
            WsRocketLeague.__subscribers[channel][event].forEach(function(callback) {
                if (_.isFunction(callback)) {
                    callback(data);
                }
            });
        }
    }
};
