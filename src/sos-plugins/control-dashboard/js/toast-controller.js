const ToastController = {
    __purgeInterval: undefined,
    show: function (title, message, color, delay) {
        delay = delay || 20000;
        let toastId = "toast-" + _.uniqueId();
        let toast = `
            <div id="${toastId}" class="toast" role="alert" data-delay="${delay}" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">
                    <span class="badge badge-${color} mr-2">&nbsp;&nbsp;</span>
                    <strong class="mr-auto">${title}</strong>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body">
                    ${message}
                </div>
            </div>
        `;
        $(".toast-insert-container").append(toast);
        $("#"+toastId).toast('show');
    },
    purgeSpent: function () {
        // Bootstrap sucks, they provide no option to autoremove toasts from DOM after it's hidden
        // this function exclusively removes toasts who have lived their lives to the fullest
        $('.toast-insert-container .toast.hide').remove();
    },
    startPurgeInterval: function () {
        this.__purgeInterval = setInterval(function () {
            ToastController.purgeSpent();
        }, 60000);
    },
    stopPurgeInterval: function () {
        clearInterval(this.__purgeInterval);
    }
};