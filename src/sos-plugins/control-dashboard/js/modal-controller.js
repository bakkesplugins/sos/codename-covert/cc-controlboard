const ModalController = {
    sizes: {
        Small: "sm",
        Large: "lg"
    },
    buttonOptions: {
        OK: {
            continue: {color:"primary", text:"OK"}
        },
        OKCancel: {
            continue: {color:"primary", text:"OK"},
            cancel: {color:"secondary", text:"Cancel"}
        },
        ContinueCancel: {
            continue: {color:"primary", text:"Continue"},
            cancel: {color:"secondary", text:"Cancel"}
        },
        YesNo: {
            continue: {color:"primary", text:"Yes"},
            cancel: {color:"secondary", text:"No"}
        },
    },
    types: {
        CustomForm: "customForm",
        Dialog: "dialog",
        NumberInput: "numberInput",
        TextInput: "textInput",
    },
    __modal_container: undefined,
    show: function (titleOrOptions, message, size, type, buttons, inputDefault) {
        if (!this.__modal_container) {
            $("body").append(`<div id="bs_modal_container"></div>`);
            this.__modal_container = $("#bs_modal_container");
        }

        let title;

        if (_.isObject(titleOrOptions)) {
            title = _.defaultTo(titleOrOptions.title, "");
            message = _.defaultTo(titleOrOptions.message, "");
            size = _.defaultTo(titleOrOptions.size, this.sizes.Small);
            buttons = _.defaultTo(titleOrOptions.buttons, this.buttonOptions.OK);
            type = _.defaultTo(titleOrOptions.type, this.types.Dialog);
            inputDefault = _.defaultTo(titleOrOptions.inputDefault, undefined);

        } else {
            title = _.defaultTo(titleOrOptions, "");
            message = _.defaultTo(message, "");
            size = _.defaultTo(size, this.sizes.Small);
            buttons = _.defaultTo(buttons, this.buttonOptions.OK);
            type = _.defaultTo(type, this.types.Dialog);
            inputDefault = _.defaultTo(inputDefault, undefined);
        }

        let _this = this;

        let buttonHtml = `<button type="button" class="btn btn-continue btn-${buttons.continue.color}" data-dismiss="modal">${buttons.continue.text}</button>`;
        if (buttons.hasOwnProperty('cancel')) {
            buttonHtml = `<button type="button" class="btn btn-cancel btn-${buttons.cancel.color}" data-dismiss="modal">${buttons.cancel.text}</button>` + buttonHtml;
        }

        let body = "";
        let inputs = [];
        let customFormId = "";
        if (type === this.types.Dialog) {
            body = message;
        } else if (type === this.types.NumberInput) {
            let iid = "iid-"+_.uniqueId();
            inputs.push(iid);
            body = `<div class="form-group">
                      <label for="${iid}">${message}</label>
                      <input type="number" class="form-control" id="${iid}" value="${inputDefault}" placeholder="${message}">
                    </div>`;
        } else if (type === this.types.TextInput) {
            let iid = "iid-"+_.uniqueId();
            inputs.push(iid);
            body = `<div class="form-group">
                      <label for="${iid}">${message}</label>
                      <input type="text" class="form-control" id="${iid}" value="${inputDefault}" placeholder="${message}">
                    </div>`;
        } else if (type === this.types.CustomForm) {
            customFormId = "fid-"+_.uniqueId();
            body = `<form id=${customFormId}>${message}</form>`
        }

        let mid = "mid-"+_.uniqueId();
        let html = `<div class="modal fade" id="${mid}" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-${size}">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">${title}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">${body}</div>
                          <div class="modal-footer">${buttonHtml}</div>
                        </div>
                        </div>
                      </div>
                    </div>`;
        this.__modal_container.append(html);
        return new Promise((resolve, reject) => {
            let $modal = $(`#${mid}`)
                .on('hidden.bs.modal', function () {
                    $modal.remove();
                    reject('user-canceled');
                })
                .modal('show');
            $modal.find(".btn-continue").click(function () {
                if (type === _this.types.CustomForm) {
                    let formData = _.keyBy($("form#"+customFormId).serializeArray(), 'name');
                    resolve(formData);
                } else if (inputs.length === 1) {
                    let v = $modal.find("#"+inputs[0]).val();
                    v.length < 1 ? reject() : resolve(v);
                } else {
                    resolve();
                }
            });
            $modal.find(".btn-cancel").click(function () {
                reject('user-canceled');
            });
        });
    }
};