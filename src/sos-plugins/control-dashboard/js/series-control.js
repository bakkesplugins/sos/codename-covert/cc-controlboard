let currentSeriesNum = 1;
let currentGameNum = 1;
let leftTeamWins = 0;
let rightTeamWins = 0;
let bestOfWinCriteria = 3;

$utils.get("#series_type").change(function () {
    let val = parseInt($utils.get("#series_type option:selected", true).val());
    if (isNaN(val)) {
        val = 0;
    }

    let inputNum = parseInt(form_data["series_type"][val]['value']);

    $utils.get("#series_current_game").attr('max', inputNum);
});

$utils.get("#series_current_game").change(function () {
    $utils.get('.selected-series-game-number').text($(this).val());
});

$utils.get("#series_current_number").change(function () {
    $utils.get('.selected-series-current-number').text($(this).val());
});

WsRocketLeague.subscribe("game", "match_ended", function(d) {
    currentGameNum++;

    let winner = d['winner_team_num'];
    if (winner === 0) {
        leftTeamWins++;
    } else if (winner === 1) {
        rightTeamWins++;
    }
    console.log('match ended');
    matchRecording.push({
        "type": "match_report",
        "team_name_left": $utils.get("#left_team_name").val(),
        "team_score_left": d['team_state'][0]['Goals'],
        "team_name_right": $utils.get("#right_team_name").val(),
        "team_score_right": d['team_state'][1]['Goals'],
    });

    if (leftTeamWins >= bestOfWinCriteria || rightTeamWins >= bestOfWinCriteria) {
        currentSeriesNum++;
        currentGameNum = 1;
        leftTeamWins = 0;
        rightTeamWins = 0;

        //finished series
        $utils.get('#series_current_number').val(currentSeriesNum);
        $utils.get('.selected-series-current-number').text(currentSeriesNum);
        $utils.get("#series_current_game").val(1);
        $utils.get('.selected-series-game-number').text(1);
        $utils.get("#series_left_team_wins").val(0);
        $utils.get("#series_right_team_wins").val(0);

        matchRecording.push({
            "type": "separator",
        });
    } else {
        $utils.get('.selected-series-game-number').text(currentGameNum);
        $utils.get("#series_current_game").val(currentGameNum);
        $utils.get("#series_left_team_wins").val(leftTeamWins);
        $utils.get("#series_right_team_wins").val(rightTeamWins);
    }
    refreshMatchReportingTable();
});