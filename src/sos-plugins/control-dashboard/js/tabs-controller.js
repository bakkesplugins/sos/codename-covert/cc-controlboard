const TabsController = {
    registeredTabs: {},

    register: function (tabObject) {
        let tab = new tabObject();

        let id = tab.id;
        let tabId = id + "-tab";
        if (this.registeredTabs.hasOwnProperty(id)) {
            console.error(`Tab with this ID has already been registered ("${id}")`, (new Error()).stack);
            return;
        }

        let navTemplateWrapper = `<div class="tab-pane fade show" id="${id}" role="tabpanel" aria-labelledby="${tabId}">
                                      ${tab.template()}
                                  </div>`;

        $("#tabContentContainer").append(navTemplateWrapper);
        tinybind.bind($("#"+id), tab.binds);

        this.registeredTabs[id] = {
            id: id,
            tabId: tabId,
            headerText: tab.headerText,
            sortPriority: _.clamp(_.defaultTo(tab.sortPriority, 999), 0, 999),
            cls: tab
        };

        tab.entry();
        this.updateTabDisplay();
    },

    updateTabDisplay: _.debounce(function() {
        let tabs = _.values(this.registeredTabs);
        tabs = _.sortBy(tabs, ["sortPriority", "headerText"]);

        let $tabContentContainer = $("#tabPillContainer");
        $tabContentContainer.html('');
        _.each(tabs, function (tab, i) {
            let active = i === 0 ? 'active' : '';
            let navItem = `<li class="nav-item">
                               <a class="nav-link ${active}" id="${tab.tabId}" data-toggle="tab" href="#${tab.id}" role="tab" aria-controls="${tab.id}" aria-selected="true">${tab.headerText}</a>
                           </li>`;
            $tabContentContainer.append(navItem);
            if (active) {
                $("#"+tab.id).tab('show');
            }
        });
    }, 500)
};