const form_data = {};

function formDataToOptions(key) {
    if (form_data.hasOwnProperty(key)) {
        let opts = "";
        _.each(form_data[key], function (e) {
            opts += `<option value="${e.value}">${e.display}</option>`;
        });
        return opts;
    }
}

function formDataGetObject(key, idx) {
    if (!_.isNumber(idx)) {
        idx = _.toNumber(idx);
    }
    if (form_data.hasOwnProperty(key)) {
        return form_data[key][idx];
    }
    return undefined;
}

function formDataGetByValue(key, value) {
    let out = undefined;
    if (form_data.hasOwnProperty(key)) {
        let skip = false;
        _.each(form_data[key], function (e) {
            if (!skip && e.value == value) {
                skip = true;
                out = e;
            }
        });
    }
    return out;
}