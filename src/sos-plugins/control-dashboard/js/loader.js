const loader = {
    __activeLoads: 0,
    begin: function () {
        this.__activeLoads++;
        this.updateText();
        $(".loader-overlay").removeClass('hidden');
    },
    end: function () {
        if (--this.__activeLoads < 1) {
            $(".loader-overlay").addClass('hidden');
        }
        this.updateText();
    },
    updateText: function () {
        $(".loader-number-left").text(this.__activeLoads);
    }
};