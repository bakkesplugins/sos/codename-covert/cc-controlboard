// Easy to use templating engine made specifically for this project. Code not lifted from anywhere

const path = require('path');
const fs = require('fs');

//======================================================================================================================
// Functions
const requireRegex = /\${require\(['"]([a-zA-Z0-9\-_.\\/]+)['"]\)}/g;

function loadFile(filePath) {
    if (!fs.existsSync(filePath)) {
        Log("qTemplate:LoadFile", `Input file not found: "${filePath}"`);
        return false;
    }
    let inputDir = filePath.substring(0, filePath.lastIndexOf('/'));
    let inputFileContents = fs.readFileSync(filePath, {
        encoding: 'utf8'
    });

    return {
        directory: inputDir,
        contents: inputFileContents
    }
}

function injectContentsRecursively(inputFilePath) {
    let initialLoad = loadFile(inputFilePath);
    if (!initialLoad) {
        process.exit(1);
    }
    let out = findInjectables(initialLoad.directory, initialLoad.contents);
    if (!out) {
        process.exit(1);
    }
    return out;
}

function findInjectables(fileDirectory, fileContents) {
    let match;
    let injects = {};
    while ((match = requireRegex.exec(fileContents)) !== null) {
        Log("qTemplate:findInjectables", "Found file to inject " + match[1]);
        injects[match[0]] = "undefined";
        let fullPath = path.resolve(fileDirectory + "\\" + match[1]);
        let injectableContents = loadFile(fullPath);
        if (injectableContents) {
            injects[match[0]] = injectableContents.contents;
        } else {
            Log("qTemplate:findInjectables", `Failed to load file in template: "${fullPath}"`);
            return false;
        }
    }

    for (let key in injects) {
        if (!injects.hasOwnProperty(key)) {
            continue;
        }
        Log("qTemplate:findInjectables:Injection", "Injecting contents into " + key);
        fileContents = fileContents.replace(key, injects[key]);
    }

    return fileContents;
}

//======================================================================================================================
// Main

function Log(action, message) {
    console.log("   [" + action + "] " + message);
}

Log("qTemplate", "Starting template generation");
let inputFile = process.argv[2];
let outputFile = process.argv[3];

let compiledContents = injectContentsRecursively(inputFile);
let savePath = path.resolve(outputFile);

Log("qTemplate:Generation", "Saving generated file to " + savePath);
fs.writeFileSync(savePath, compiledContents);
Log("qTemplate", "Generation finished");